using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TablaDePrestamos
{
    class Program
    {
        static void Main(string[] args)
        {

            float Pagos, InteresPagado, CapitalPagado, MontoPrestamo, Interes, InteresMensual;
            int  Plazo;

            Console.Write("Introduce el monto del prestamo: ");
            MontoPrestamo = float.Parse(Console.ReadLine());
            Console.Write("Introduce la tasa de interes anual: ");
            Interes = float.Parse(Console.ReadLine());
            Console.Write("Introduce el plazo en meses: ");
            Plazo = int.Parse(Console.ReadLine());
            
            
            InteresMensual = (Interes / 100) / 12;
            Pagos = InteresMensual + 1;
            Pagos = (float)Math.Pow(Pagos, Plazo);
            Pagos = Pagos - 1;
            Pagos = InteresMensual / Pagos;
            Pagos = InteresMensual + Pagos;
            Pagos = MontoPrestamo * Pagos;



           
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.Write(" Numero de pago \t");
            Console.Write("Pago \t\t");
            Console.Write("Intereses Pagados \t\t");
            Console.Write("Capital Pagado \t\t");
            Console.Write("Monto del prestamo \t");
            Console.WriteLine();
            Console.WriteLine();
           


            for (int i = 1; i <= Plazo; i++)
            {

                
                Console.Write($"\t{i}\t\t");

               
                Console.Write($"{Pagos}\t");

               
                InteresPagado = InteresMensual * MontoPrestamo;
                Console.Write($"{InteresPagado}\t\t");

               
                CapitalPagado = Pagos - InteresPagado;
                Console.Write($"\t{CapitalPagado}\t\t");

               
                MontoPrestamo = MontoPrestamo - CapitalPagado;
                Console.Write($"\t{MontoPrestamo}\t");

                Console.WriteLine();
                

            }
        }
    }
}
